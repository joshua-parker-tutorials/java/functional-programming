package com.joshparker.tutorials.java.functionalprogramming.pureFunctions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A function is a pure function if both conditions are met:
 *
 * - The function has no side effects (does/effects instance variables, writing to a file, database call...).
 * - The return value depends only on the input parameters passed to the function.
 *
 * - (optional) favor recursion over looping
 */
public class PureFunctionsClass {

    public void showPureFunctionsExample() {
        int addition = sum(20, 30);
        System.out.println(addition);

        String string = "This is a sentence with a desire to replace all 'a' with a @";
        String newString = replaceCharSequence(string, "@", "a");
        System.out.println("Before: " + string);
        System.out.println("After: " + newString);

        List<Customer> customers = Arrays.asList(
                new Customer("josh", "parker", "someemail@hotmail.com", 23),
                new Customer("Lola", "Parker", "dogsdonthaveemail@gmail.com", 7),
                new Customer("Mickey", "Parker", "dogsstilldonthaveemail@outlook.com", 18));
        List<Customer> customersByAge = getAllCustomersByAgeGreaterThan(customers, 10);
        customersByAge.forEach(customer -> System.out.println(customer.toString()));
    }

    private int sum(int a, int b) {
        return a + b;
    }

    private String replaceCharSequence(String string, String newCharSequence, String oldCharSequence) {
        return string.replace(oldCharSequence, newCharSequence);
    }

    private List<Customer> getAllCustomersByAgeGreaterThan(List<Customer> customers, int age) {
        return customers.stream()
                .filter(customer -> customer.getAge() > age)
                .collect(Collectors.toList());
    }
}
