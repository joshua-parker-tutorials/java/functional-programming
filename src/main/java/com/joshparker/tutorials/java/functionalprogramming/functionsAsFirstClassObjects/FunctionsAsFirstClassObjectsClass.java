package com.joshparker.tutorials.java.functionalprogramming.functionsAsFirstClassObjects;

public class FunctionsAsFirstClassObjectsClass {

    /**
     *
     * In the functional programming paradigm, functions are first class objects in the language.
     * That means that you can create an "instance" of a function, as have a variable reference that function instance,
     * just like a reference to a String, Map or any other object. Functions can also be passed as parameters to other
     * functions. In Java, methods are not first class objects. The closest we get is Java Lambda expressions.
     *
     */
    public void showFunctionsAsFirstClassObjectsExample() {
        System.out.println("As this would be an example of lambda expression, it deserves it's own tutorial");
    }
}
