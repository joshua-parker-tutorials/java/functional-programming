package com.joshparker.tutorials.java.functionalprogramming.higherOrderFunctions;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A function is a higher order function if at least one of the following conditions are met:
 * - The function takes one or more functions as parameters.
 * - The function returns another function as result.
 *
 */
public class HigherOrderFunctionsClass {

    private static final List<Post> POST_LIST = Arrays.asList(
            new Post("OMG! My cat just jumped off the house!", LocalDate.of(2010, 1, 22)),
            new Post("I make the best chicken fajitas EVER!", LocalDate.of(2014, 3, 16)),
            new Post("I can't believe Dog spelt backwards is God", LocalDate.of(2016, 7, 12)),
            new Post("What is 'Time'?", LocalDate.of(2019, 4, 19))
    );

    private static final Predicate<Post> isPostAfter2014 =
            post -> post.getCreationDate().isAfter(LocalDate.of(2014, 12, 31));

    public void showHigherOrderFunctionsExample() {
        Function<String, String> startComputer = startComputer();
        System.out.println(startComputer.apply("Mac OS X"));
        System.out.println(startComputer.apply("Windows"));
        System.out.println(startComputer.apply("Linux"));

        List<Post> allPosts = removeExpiredPosts(isPostAfter2014);
        allPosts.forEach(System.out::println);
    }

    /** Example of function returning a function **/
    private Function<String, String> startComputer() {
        return s -> s + " operating system is booting";
    }

    /** Example of a function taking a function as parameter **/
    private List<Post> removeExpiredPosts(Predicate<Post> isPostInDate) {
        return POST_LIST
                .stream()
                .filter(isPostInDate)
                .collect(Collectors.toList());
    }

}
