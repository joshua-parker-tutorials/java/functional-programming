package com.joshparker.tutorials.java.functionalprogramming.functionalComposition;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class FunctionalCompositionClass {

    private static final List<Customer> CUSTOMER_LIST = Arrays.asList(
            new Customer("Josh", "parker", "someemailhotmail.com"),
            new Customer("Lola", "Parker", "dogsdonthaveemail@gmail.com"),
            new Customer("Mickey", "Parker", "dogsstilldonthaveemail@outlook.com"),
            new Customer("Co", "clarke", "codie@outlook.com"),
            new Customer("kerry", "Pa", "kerry@outlook.com"));

    public void showFunctionalCompositionExample() {
        CUSTOMER_LIST.forEach(customer ->
                System.out.println(customer.getFirstName() + " has valid credentials: " + isCustomerValid.test(customer)));
    }

    private Predicate<Customer> isCustomerValid = customer -> {
        Predicate<String> isBetweenMinAndMaxLength = (input) -> input.length() > 2 && input.length() < 20;
        Predicate<String> startsWithLetter = string -> Character.isAlphabetic(string.charAt(0));
        Predicate<String> endsWithLetter = string -> Character.isAlphabetic(string.charAt(string.length()-1));
        Predicate<String> containsOnlyOneAtSymbol = string -> string.contains("@");

        Predicate<String> isFirstNameValid = firstName -> startsWithLetter.test(firstName) &&
                                                            endsWithLetter.test(firstName) &&
                                                            isBetweenMinAndMaxLength.test(firstName);

        Predicate<String> isLastNameValid = lastName -> startsWithLetter.test(lastName) &&
                                                            endsWithLetter.test(lastName) &&
                                                            isBetweenMinAndMaxLength.test(lastName);

        Predicate<String> isEmailValid = email -> startsWithLetter.test(email) &&
                                                    endsWithLetter.test(email) &&
                                                    containsOnlyOneAtSymbol.test(email);

        return isFirstNameValid.test(customer.getFirstName()) &&
                isLastNameValid.test(customer.getLastName()) &&
                isEmailValid.test(customer.getEmail());
    };

}
