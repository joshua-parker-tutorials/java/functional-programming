package com.joshparker.tutorials.java.functionalprogramming;

import com.joshparker.tutorials.java.functionalprogramming.functionalComposition.FunctionalCompositionClass;
import com.joshparker.tutorials.java.functionalprogramming.functionalInterfaces.FunctionalInterfaceClass;
import com.joshparker.tutorials.java.functionalprogramming.functionsAsFirstClassObjects.FunctionsAsFirstClassObjectsClass;
import com.joshparker.tutorials.java.functionalprogramming.higherOrderFunctions.HigherOrderFunctionsClass;
import com.joshparker.tutorials.java.functionalprogramming.pureFunctions.PureFunctionsClass;

/**
 * Functional programming contains the following key concepts:
 * - Functions as first class objects
 * - Pure functions
 * - Higher order functions
 *
 * Functional programming must follow these rules:
 * - No state
 * - No side effects
 * - Immutable variables
 * - Favour recursion over looping
 *
 * Other concepts include:
 * - Functional interfaces
 * - Functional composition
 */
public class FunctionalProgrammingTutorial {
    public static void main(String[] args) {
        System.out.println("\n===== Pure Functions Example =====");
        PureFunctionsClass pureFunctionsClass = new PureFunctionsClass();
        pureFunctionsClass.showPureFunctionsExample();

        System.out.println("\n===== Functions as First Class Objects Example =====");
        FunctionsAsFirstClassObjectsClass funcsAsFirstClassObjectsClass = new FunctionsAsFirstClassObjectsClass();
        funcsAsFirstClassObjectsClass.showFunctionsAsFirstClassObjectsExample();

        System.out.println("\n===== Higher Order Functions Example =====");
        HigherOrderFunctionsClass higherOrderFunctionsClass = new HigherOrderFunctionsClass();
        higherOrderFunctionsClass.showHigherOrderFunctionsExample();

        System.out.println("\n===== Functional Interface Example =====");
        FunctionalInterfaceClass functionalInterfaceClass = new FunctionalInterfaceClass();
        functionalInterfaceClass.showFunctionalInterfaceExample();

        System.out.println("\n===== Functional Composition Example =====");
        FunctionalCompositionClass functionalCompositionClass = new FunctionalCompositionClass();
        functionalCompositionClass.showFunctionalCompositionExample();
    }
}
