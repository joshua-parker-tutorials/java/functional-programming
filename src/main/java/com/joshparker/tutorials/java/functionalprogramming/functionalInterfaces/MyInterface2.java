package com.joshparker.tutorials.java.functionalprogramming.functionalInterfaces;

public interface MyInterface2 {
    String removeSpecialCharacters(String string);
}
