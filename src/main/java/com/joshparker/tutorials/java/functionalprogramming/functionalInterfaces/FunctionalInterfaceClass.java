package com.joshparker.tutorials.java.functionalprogramming.functionalInterfaces;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * The term Java functional interface was introduced in Java 8.
 * A functional interface in Java is an interface that contains
 * only a single abstract method. A functional
 * interface can contain default and static methods which do have
 * an implementation, in addition to the single unimplemented method.
 *
 * An advantage of Functional Interfaces is we can provide implementation to our interfaces with lambda expression.
 *
 * Java has many ready made Functional interfaces, commonly used ones include:
 * - Supplier (Takes no parameters and 'suppliers' us with a value)
 * - Consumer (Takes/'consumers' a single parameter and performs a given action. Returns void)
 * - Function (Takes one parameter and produces a result.
 *              Therefore can be used as the assignment target for a lambda expression or method reference.
 *              As Functions can are capable of being first class objects, you can pass these about as you
 *              would a String, List, Map...
 *             )
 * - Predicate (Takes one parameter. Represents a boolean-valued function of one argument)
 *
 */
public class FunctionalInterfaceClass {

    public void showFunctionalInterfaceExample() {
        firstExample();
        secondExample();
        thirdExample();

    }

    private void firstExample() {
        MyInterface print = System.out::println;
        print.print("Hello");
    }

    private void secondExample() {
        MyInterface2 cleanString = s -> s.replaceAll("[^a-zA-Z0-9\\s+]", "");

        String dirtyString = "This is@ a (string)'/ with special characters!]";
        String response = cleanString.removeSpecialCharacters(dirtyString);
        System.out.println(response);
    }

    private void thirdExample() {
        MyFunction3 filterNumbers = (list, max) ->
                list.stream()
                .filter(num -> num > max)
                .collect(Collectors.toList());

        List<Integer> numbers = Arrays.asList(99, 63, 75, 12, 42, 100, 28);
        List<Integer> numsLargerThen50 = filterNumbers.extractNumbersGreaterThan(numbers, 50);
        System.out.println(numsLargerThen50);
    }
}
