package com.joshparker.tutorials.java.functionalprogramming.higherOrderFunctions;

import java.time.LocalDate;

public class Post {
    private String content;
    private LocalDate creationDate;

    public Post(String content, LocalDate creationDate) {
        this.content = content;
        this.creationDate = creationDate;
    }

    public String getContent() {
        return content;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    @Override
    public String toString() {
        return "Post{" +
                "content='" + content + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
