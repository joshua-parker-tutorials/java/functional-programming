package com.joshparker.tutorials.java.functionalprogramming.functionalInterfaces;

import java.util.List;

public interface MyFunction3 {
    List<Integer> extractNumbersGreaterThan(List<Integer> list, int max);
}
