package com.joshparker.tutorials.java.functionalprogramming.functionalInterfaces;

public interface MyInterface {

    void print(String string);
}
